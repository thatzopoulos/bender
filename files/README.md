Arduino code for inspecting the 6502 bus from the first video: 6502-monitor.ino
Python script for generating the ROM image with the LED blink program from the second video: makerom.py
Assembly program for blinking LEDs from the third video: blink.s
Long and inefficient "hello world" assembly program from the fourth video: hello-world.s
Final "hello world" assembly program from the last video: hello-world-final.s

