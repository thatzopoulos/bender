# Bender: Homemade 6502 Computer
![My Bender 6502](img/bender-6502.jpeg)

This is a repo to keep track of code, notes, and datasheets related to my build of Ben Eater's 6502 Computer Kit.

## Computer Stats? Facts?
### Memory Map
| Chip | Range | Description |
| --- | --- | --- |
| RAM | `$0000` - `$00ff` | Zero page |
| RAM | `$0100` - `$01ff` | Stack |
| RAM | `$0200` - `$3fff` | General RAM |
| Open | `$4000` - `$5fff` | Not mapped to a device |
| 6522 | `$6000` | I/O Register B |
| 6522 | `$6001` | I/O Register A |
| 6522 | `$6002` | Data Direction Register B |
| 6522 | `$6003` | Data Direction Register A |
| 6522 | `$6004` | T1 Low Order Latches/Counter |
| 6522 | `$6005` | T1 High Order Counter |
| 6522 | `$6006` | T1 Low Order Latches |
| 6522 | `$6007` | T1 High Order Latches |
| 6522 | `$6008` | T2 Low Order Latches/Counter |
| 6522 | `$6009` | T2 High Order Counter |
| 6522 | `$600a` | Shift Register |
| 6522 | `$600b` | Auxiliary Control Register |
| 6522 | `$600c` | Peripheral Control Register |
| 6522 | `$600d` | Interrupt Flag Register |
| 6522 | `$600e` | Interrupt Enable Register |
| 6522 | `$600f` | I/O Register A sans Handshake |
| 6522 | `$6010` - `7fff` | Mirrors of the sixteen VIA registers |
| 28C256 | `$8000` - `$ffff` | ROM |

Info from: https://www.reddit.com/r/beneater/comments/doytpo/6502_project_memory_map/


## Hardware Requirements
This code will work on BE6502 computers that have been built up to the stage before VGA is added.

## Software Requirements
The code in this repository is compiled with VASM oldstyle. You can download VASM at http://www.ibaug.de/vasm/vasm6502.zip
```
wget http://sun.hasenbraten.de/vasm/release/vasm.tar.gz 
tar xvzf vasm.tar.gz 
cd vasm 
make CPU=6502 SYNTAX=oldstyle 
cp vasm6502_oldstyle /usr/bin 
cp vobjdump /usr/bin
```
https://www.arduino.cc/en/software Arduino IDE from here
Open Source Program for controlling the MiniPRO TL866xx chip programmer: https://gitlab.com/DavidGriffith/minipro
```
git clone https://gitlab.com/DavidGriffith/minipro.git 
cd minipro
make
make install
```

## Repo Structure
```
datasheets : Datasheets for various hardware parts of this project, and some manuals for tools I use like the AN8008 multimeter
files: Code I used in the project so far, has more info in the folder's readme file
```
Individual project files and libraries should have descriptions at the top of the file

## Compiling Stuff
To assembly (compile) a project, copy any necessary library files int o the build directory with the project's source. Also, copy the vasm binary into the folder.
Possible example command:
``` 
vasm -Fbin -dotdir ./filename -o outputfilename
```

## Changes from Ben Eater 6502 Base Kit

## Notes for each Ben Eater video

### Clock Module Videos
No code, just building a clock. Might have to make changes to the switch the kit came with but so far its exactly as it is in the video/schematic.

### Video 1
No commands for this one, just reading in data from hardwared stuff.


## Other repos of interest:
https://github.com/transitorykris/krisos -> intense lcd

https://github.com/foliagecanine/my6502 -> Has the google dinosaur game running on the lcd

https://github.com/dbuchwald/6502 -> Different than bens but has good software examples that can run on BE6502 compatible builds

https://github.com/janroesner/sixty5o2

https://github.com/caseywdunn/eater_6502 -> Good Video breakdowns in the readme

## Credits
Inspiration / Support / Help:
 - https://eater.net/ used his kit and video series to build my computer
 - http://wilsonminesco.com/6502primer/
